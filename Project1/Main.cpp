#include <iostream>
#include <string>

class Animal {
public:
	virtual void Voice() {
		std::cout << "Voice!\n";
	}
};

class Dog : public Animal {
public:
	void Voice() override {
		std::cout << "Woof!\n";
	}
};

class Cat : public Animal {
public:
	void Voice() override {
		std::cout << "Mew!\n";
	}
};

class Mouse : public Animal {
public:
	void Voice() override {
		std::cout << "Squeak!\n";
	}
};

int main() {
	Mouse m;
	Cat c;
	Dog d;

	Animal* animal[3] = { &m,&c,&d };

	for (Animal* i : animal) {
		i->Voice();
	}
}